/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Hashtable;
import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class BinHexa {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        Hashtable tabla  = new Hashtable();
        Scanner entrada = new Scanner(System.in);
        String bin1;
        String bin2;
        String bin3;
        String bin4;
        
	tabla.put("0000","0");
        tabla.put("0001","1");
        tabla.put("0010","2");
        tabla.put("0011","3");
        tabla.put("0100","4");
        tabla.put("0101","5");
        tabla.put("0110","6");
        tabla.put("0111","7");
        tabla.put("1000","8");
        tabla.put("1001","9");
        tabla.put("1010","A");
        tabla.put("1011","B");
        tabla.put("1100","C");
        tabla.put("1101","D");
        tabla.put("1110","E");
        tabla.put("1111","F");
        
       System.out.println("Conversion de Binario a Hexadecimal por separacion de 4 bits");
       System.out.println("Ingrese el primer numero binario a convertir");
       bin1=entrada.next();
       System.out.println("Ingrese el segundo numero binario a convertir");
       bin2=entrada.next();
       System.out.println("Ingrese el tercer numero binario a convertir");
       bin3=entrada.next();
       System.out.println("Ingrese el cuarto numero binario a convertir");
       bin4=entrada.next();
     
     if(bin1.length() == 4 && bin2.length() == 4 && bin3.length() == 4 && bin4.length() == 4){
     String hexa1 = (String) tabla.get(bin1);
     String hexa2 = (String) tabla.get(bin2);
     String hexa3 = (String) tabla.get(bin3);
     String hexa4 = (String) tabla.get(bin4);
     
     System.out.println("El resultado de la conversion es: " +hexa1+hexa2+hexa3+hexa4);
     }
     else
     {
        System.err.println("Los numeros binarios deben de cumplir un tamaño de 4 bits"); 
     }
    }
    
}
